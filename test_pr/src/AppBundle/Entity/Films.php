<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="films")
 */
class Films{
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2,max=250)
     * @ORM\Column(type="string", length=250)
     */
    public $name;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Assert\Length(max=4)
     * @Assert\Regex("/([0-9]{4})/")
     */
    public $year;
    
    /**
     * @Assert\NotBlank()
     * @Assert\Choice(
     *     choices = { 0,1 },
     *     message = "Choose a valid gender."
     * )
     * @ORM\Column(type="integer",name="isActive")
     */
    public $isActive;
    
}
