<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Tools\Pagination\Paginator;

use AppBundle\Entity\Films;
use AppBundle\Entity\FilmsForm;

/**
 * @author orson
 */
class FilmsController extends \Symfony\Bundle\FrameworkBundle\Controller\Controller/**/{
    
    /**
     * @Route("/films")
     */
    public function indexAction(Request $request){
        $admin = (int)$request->get("admin",0);
        //var_dump($admin);
        $em    = $this->get('doctrine.orm.entity_manager');
        $endStr = $admin?"":" WHERE a.isActive = 1 ";
        $dql   = "SELECT a FROM AppBundle:Films a " . $endStr . " ORDER BY a.id DESC";
        //var_dump($dql);
        $query = $em->createQuery($dql);
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );
        
        return $this->render("films/index.html.twig", [
            //"test"=>array(1,2,3),
            "pagination"=>$pagination,
            "admin"=>$admin,
            ]);
    }
    
    /**
     * @Route("/films/delete/{id}")
     */
    public function deleteAction(Request $request) {
        $id = (int)$request->get("id");
        $filmsModel = $this->getRepo("films")->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($filmsModel);
        $em->flush();
        //var_dump($this->container->get('router')->getContext()->getBaseUrl());die;
        return $this->redirect($this->container->get('router')->getContext()->getBaseUrl() . "/films?admin=1");
    }
    
    private function getRepo($name = ""){
        if($name){
           return $this->getDoctrine()->getRepository('AppBundle:' . ucfirst($name));
        }
        return false;
    }
    
    /**
     * @Route("/films/edit/{id}",defaults={"id"="new"})
     */
    public function editAction(Request $request){
        if("new" !== ($id = $request->get("id"))){
            //$repos = $this->getDoctrine()->getRepository("AppBundle:Films");
            $repos = $this->getRepo("films");
            $filmsModel = $repos->find((int)$id);
        }else{
            $filmsModel = new Films();
        }
        $form = $this->createForm(FilmsForm::class, $filmsModel);
        $form->handleRequest($request);
        var_dump($form->isSubmitted());
        var_dump($form->isValid());
        //var_dump($form->getErrors());
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($filmsModel);
            $em->flush();
            $this->addFlash(
                'notice',
                'Your changes saved!'
            );
            //return $this->redirect("edit/" . $filmsModel->id);
        }
        return $this->render("films/edit.html.twig", [
            "form"=>$form->createView(),
            ]);
    }
}
