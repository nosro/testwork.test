<?php
/*
 * first task
 * time = 13:47m
 */
function setRandValInArray(array &$array,$n){
    if(!is_int($n))throw new Exception("N must be integer");
    if($n !== 0){
        $array[] = rand();
        setRandValInArray($array,$n-1);
    }
}


try{
?>
<form method="post">
    <input type="number" name="n" /><br>
    <input type="submit" value="Send" />
</form>
<?php
    if($_SERVER["REQUEST_METHOD"] == "POST" && $n = (int)trim($_POST["n"])){
        $randArray = [];
        setRandValInArray($randArray, $n);
        //$randArray = array_rand($randArray,5);
        echo "<pre>";
        var_dump($randArray);
        echo "</pre>";
    }
} catch (Exception $ex) {
    echo "Error<br>";
    print_r($ex->getMessage());
}