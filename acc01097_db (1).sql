-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Хост: acc01097.mysql.ukraine.com.ua
-- Время создания: Мар 06 2016 г., 17:03
-- Версия сервера: 5.6.27-75.0-log
-- Версия PHP: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `acc01097_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `films`
--

CREATE TABLE IF NOT EXISTS `films` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(250) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Дамп данных таблицы `films`
--

INSERT INTO `films` (`id`, `name`, `year`, `isActive`) VALUES
(1, 'test1', 1123, 1),
(2, '123123', 2123, 0),
(3, '123', 4234, 1),
(4, '123', 2234, 0),
(15, 'test3', 1111, 0),
(16, '123123', 1233, 0),
(17, 'test1', 1111, 1),
(19, '123123', 1244, 1),
(20, '123123', 1244, 1),
(21, '123123', 1244, 1),
(26, 'test3', 1111, 1),
(27, 'test1', 1111, 1),
(28, '123123', 1111, 1),
(29, 'test1', 1111, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
